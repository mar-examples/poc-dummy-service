package com.mapfre.gaia.dummyservice.controller;

import com.mapfre.gaia.dummyservice.domain.DummyResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * Dummy-service REST Controller
 */
@RestController
@RequestMapping("/api/standby")
public class DummyController {

    private static final long DEFAULT_SERVICE_STANDBY = 120000;
    private static final String SUCCESS_RESPONSE_MSG = "Response from Gaia dummy-service";

    /**
     * Endpoint that returns an dummy response
     * @param standby time (in milliseconds) that the service will stands
     * @return JSON-formatted response after defined or default time. Error response in case of exception
     */
    @GetMapping
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DummyResponse> dummyStandby(@RequestParam(required = false, name = "duration") Long standby){
        try {
            if (standby.equals(null) || standby.equals(0)){
                //Wait default standby
                TimeUnit.MILLISECONDS.sleep(DEFAULT_SERVICE_STANDBY);
                return new ResponseEntity<>(
                        new DummyResponse("success", DEFAULT_SERVICE_STANDBY, SUCCESS_RESPONSE_MSG), HttpStatus.OK);
            }
            TimeUnit.MILLISECONDS.sleep(standby);
            return new ResponseEntity<>(
                    new DummyResponse("success", standby, SUCCESS_RESPONSE_MSG), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(
                    new DummyResponse("error", null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
