package com.mapfre.gaia.dummyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummyserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyserviceApplication.class, args);
	}

}
