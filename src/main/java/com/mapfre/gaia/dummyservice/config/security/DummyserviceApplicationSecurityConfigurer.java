package com.mapfre.gaia.dummyservice.config.security;

import com.mapfre.dgtp.gaia.commons.security.annotation.GaiaSecurityConfigHttp;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class DummyserviceApplicationSecurityConfigurer implements GaiaSecurityConfigHttp {
    @Override
    public void configure(HttpSecurity httpSecurity, AuthenticationSuccessHandler authenticationSuccessHandler) throws Exception {
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api").permitAll();
    }
}
