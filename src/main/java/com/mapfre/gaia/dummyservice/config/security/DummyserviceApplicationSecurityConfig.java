package com.mapfre.gaia.dummyservice.config.security;

import com.mapfre.dgtp.gaia.commons.security.annotation.AbstractGaiaSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@EnableWebSecurity
public class DummyserviceApplicationSecurityConfig extends AbstractGaiaSecurity {

    private DummyserviceApplicationSecurityConfigurer applicationSecurityConfigurer;

    public DummyserviceApplicationSecurityConfig(DummyserviceApplicationSecurityConfigurer applicationSecurityConfigurer) {
        this.applicationSecurityConfigurer = applicationSecurityConfigurer;
    }

    @Override
    protected void configureAuthorizeRequests(HttpSecurity http) throws Exception {
        applicationSecurityConfigurer.configure(http, null);
    }
}
