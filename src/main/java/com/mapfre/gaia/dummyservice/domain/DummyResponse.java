package com.mapfre.gaia.dummyservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Service Response POJO
 */
@Data
@AllArgsConstructor
public class DummyResponse {
    private String status;
    private Long standby;
    private String data;
}
