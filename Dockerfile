FROM openjdk:8-jdk-alpine
ENV JAVA_OPTIONS "-Xms256m -Xmx512m -Djava.awt.headless=true"
RUN addgroup bootapp && \
    adduser -D -S -h /var/cache/bootapp -s /sbin/nologin -G bootapp bootapp
USER bootapp
ARG JAR_FILE=*.jar
COPY ${JAR_FILE} /opt/app.jar
ENTRYPOINT ["java","-jar","/opt/app.jar"]