# Dummy-service #

This simple application serves an _dummy_ JSON delayed response. Delay can be configured with 
`duration` request param.

This is a testing purpose application consuming by [PoC - Timeouts on RestClients](https://bitbucket.org/mar-examples/poc-timeout-restclient/src/master/)  

### Endpoints ###

    GET /api/standby
    Get a JSON response after configured delay (default 120 secs)
    URL Params: 
        duration=<timeMiliseconds> Configure custom delay


### Build and run ###

This is a Gaia-boot application so can be easily built and run using `spring-boot-maven-plugin`:

```
 org.springframework.boot:spring-boot-maven-plugin:run
```

Also, you can build a `.jar` package and execute `java -jar` command to run the application.

At the moment, this service is published at: http://les000a103050.es.mapfre.net:8680/dummyservice/api/standby